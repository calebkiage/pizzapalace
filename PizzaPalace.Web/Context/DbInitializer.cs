﻿using PizzaPalace.Web.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaPalace.Web.Context
{
    public static class DbInitializer
    {
        public static void Initialize(PizzaPalaceDbContext db)
        {
            db.Database.EnsureCreated();

            // Check if we seeded the database.
            if (db.Sizes.Any())
            {
                return;
            }

            var size1 = new Size { Name = "Small" };
            var size2 = new Size { Name = "Medium" };
            var size3 = new Size { Name = "Large" };
            db.Add(size1);
            db.Add(size2);
            db.Add(size3);

            // Save first coz we need access to the size ids.
            db.SaveChanges();

            var pizza1 = new Product { Type = ProductType.Pizza };
            db.Add(pizza1);
            // Add prices and amount.
            db.Add(new ProductSize { Amount = 12, Product = pizza1, Size = size1 });
            db.Add(new ProductSize { Amount = 14, Product = pizza1, Size = size2 });
            db.Add(new ProductSize { Amount = 16, Product = pizza1, Size = size3 });


            // Add toppings.
            // Basic toppings.
            var topping1 = new Product { Name = "Cheese", Type = ProductType.BasicTopping };
            db.Add(new ProductSize { Amount = 0.5m, Product = topping1, Size = size1 });
            db.Add(new ProductSize { Amount = 0.75m, Product = topping1, Size = size2 });
            db.Add(new ProductSize { Amount = 1, Product = topping1, Size = size3 });
            db.Add(topping1);


            var topping2 = new Product { Name = "Pepperoni", Type = ProductType.BasicTopping };
            db.Add(new ProductSize { Amount = 0.5m, Product = topping2, Size = size1 });
            db.Add(new ProductSize { Amount = 0.75m, Product = topping2, Size = size2 });
            db.Add(new ProductSize { Amount = 1, Product = topping2, Size = size3 });
            db.Add(topping2);


            var topping3 = new Product { Name = "Ham", Type = ProductType.BasicTopping };
            db.Add(new ProductSize { Amount = 0.5m, Product = topping3, Size = size1 });
            db.Add(new ProductSize { Amount = 0.75m, Product = topping3, Size = size2 });
            db.Add(new ProductSize { Amount = 1, Product = topping3, Size = size3 });
            db.Add(topping3);


            var topping4 = new Product { Name = "Pineapple", Type = ProductType.BasicTopping };
            db.Add(new ProductSize { Amount = 0.5m, Product = topping4, Size = size1 });
            db.Add(new ProductSize { Amount = 0.75m, Product = topping4, Size = size2 });
            db.Add(new ProductSize { Amount = 1, Product = topping4, Size = size3 });
            db.Add(topping4);


            // Deluxe toppings
            var topping5 = new Product { Name = "Sausage", Type = ProductType.DeluxeTopping };
            db.Add(new ProductSize { Amount = 2, Product = topping5, Size = size1 });
            db.Add(new ProductSize { Amount = 3, Product = topping5, Size = size2 });
            db.Add(new ProductSize { Amount = 4, Product = topping5, Size = size3 });
            db.Add(topping5);

            var topping6 = new Product { Name = "Feta Cheese", Type = ProductType.DeluxeTopping };
            db.Add(new ProductSize { Amount = 2, Product = topping6, Size = size1 });
            db.Add(new ProductSize { Amount = 3, Product = topping6, Size = size2 });
            db.Add(new ProductSize { Amount = 4, Product = topping6, Size = size3 });
            db.Add(topping6);

            var topping7 = new Product { Name = "Tomatoes", Type = ProductType.DeluxeTopping };
            db.Add(new ProductSize { Amount = 2, Product = topping7, Size = size1 });
            db.Add(new ProductSize { Amount = 3, Product = topping7, Size = size2 });
            db.Add(new ProductSize { Amount = 4, Product = topping7, Size = size3 });
            db.Add(topping7);

            var topping8 = new Product { Name = "Olives", Type = ProductType.DeluxeTopping };
            db.Add(new ProductSize { Amount = 2, Product = topping8, Size = size1 });
            db.Add(new ProductSize { Amount = 3, Product = topping8, Size = size2 });
            db.Add(new ProductSize { Amount = 4, Product = topping8, Size = size3 });
            db.Add(topping8);

            db.SaveChanges();
        }
    }
}
