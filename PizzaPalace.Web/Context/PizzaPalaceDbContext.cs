﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PizzaPalace.Web.Entities;

namespace PizzaPalace.Web.Context
{
    public class PizzaPalaceDbContext : DbContext
    {
        public PizzaPalaceDbContext(DbContextOptions options) : base(options){ }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderDetail> OrderDetails { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Size> Sizes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductSize>().HasKey(ps => new { ps.ProductId, ps.SizeId });
            modelBuilder.Entity<ProductSize>().HasOne(ps => ps.Size).WithMany(ps => ps.ProductSizes).HasForeignKey(ps => ps.SizeId);
            modelBuilder.Entity<ProductSize>().HasOne(ps => ps.Product).WithMany(ps => ps.ProductSizes).HasForeignKey(ps => ps.ProductId);
            
            modelBuilder.Entity<OrderDetailTopping>().HasKey(odt => new { odt.OrderDetailId, odt.ToppingId });
            modelBuilder.Entity<OrderDetailTopping>().HasOne(odt => odt.OrderDetail).WithMany(od => od.OrderDetailToppings).HasForeignKey(odt => odt.OrderDetailId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<OrderDetailTopping>().HasOne(odt => odt.Topping).WithMany(od => od.OrderDetailToppings).HasForeignKey(odt => odt.OrderDetailId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
