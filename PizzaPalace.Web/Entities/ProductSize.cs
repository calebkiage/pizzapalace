﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaPalace.Web.Entities
{
    public class ProductSize
    {
        public decimal Amount { get; set; }

        public Product Product { get; set; }
        public int ProductId { get; set; }

        public Size Size { get; set; }

        public int SizeId { get; set; }
    }
}
