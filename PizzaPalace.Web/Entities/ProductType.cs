﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPalace.Web.Entities
{
    public enum ProductType
    {
        Pizza,
        BasicTopping,
        DeluxeTopping
    }
}
