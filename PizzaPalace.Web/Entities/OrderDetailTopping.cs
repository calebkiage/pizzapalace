﻿namespace PizzaPalace.Web.Entities
{
    public class OrderDetailTopping
    {
        public OrderDetail OrderDetail { get; set; }

        public int OrderDetailId { get; set; }

        public Product Topping { get; set; }

        public int ToppingId { get; set; }
    }
}
