﻿using System.Collections.Generic;

namespace PizzaPalace.Web.Entities
{
    public class OrderDetail : Entity
    {
        public decimal Amount { get; set; }

        public decimal Gst { get; set; }

        public Order Order { get; set; }

        public int OrderId { get; set; }

        public Product Product { get; set; }

        public int ProductId { get; set; }

        public double Quantity { get; set; }

        public Size Size { get; set; }

        public int SizeId { get; set; }

        public virtual List<OrderDetailTopping> OrderDetailToppings { get; private set; } = new List<OrderDetailTopping>();
    }
}
