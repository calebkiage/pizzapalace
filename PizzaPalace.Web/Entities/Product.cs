﻿using System.Collections.Generic;

namespace PizzaPalace.Web.Entities
{
    public class Product : Entity
    {
        public string Name { get; set; }

        public virtual List<ProductSize> ProductSizes { get; private set; } = new List<ProductSize>();

        public virtual List<OrderDetailTopping> OrderDetailToppings { get; private set; } = new List<OrderDetailTopping>();

        public ProductType Type { get; set; }
    }
}
