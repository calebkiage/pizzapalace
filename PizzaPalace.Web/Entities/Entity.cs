﻿namespace PizzaPalace.Web.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
