﻿using System.Collections.Generic;

namespace PizzaPalace.Web.Entities
{
    public class Size : Entity
    {
        public string Name { get; set; }
        
        public virtual List<ProductSize> ProductSizes { get; private set; } = new List<ProductSize>();
    }
}
