﻿using System.Collections.Generic;

namespace PizzaPalace.Web.Entities
{
    public class Order : Entity
    {
        public virtual List<OrderDetail> OrderDetails { get; private set; } = new List<OrderDetail>();

        public decimal TotalAmount { get; set; }
    }
}
