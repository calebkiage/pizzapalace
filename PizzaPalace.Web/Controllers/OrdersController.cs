﻿using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PizzaPalace.Web.Context;
using PizzaPalace.Web.Entities;
using PizzaPalace.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaPalace.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrdersController
    {
        private readonly PizzaPalaceDbContext db;

        public OrdersController(PizzaPalaceDbContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public IEnumerable<object> GetOrders()
        {
            var orders = this.db.Orders.ToList();
            var results = new List<object>();
            foreach (var order in orders)
            {
                dynamic result = new ExpandoObject();
                var details = this.db.OrderDetails.Where(od => od.OrderId == order.Id).Select(od=> new { od.Id, od.Quantity, od.Size, od.Product }).ToList();
                result.Id = order.Id;
                result.Amount = order.TotalAmount;
                result.Label = $"{"Pizza".ToQuantity((int)details.Sum(d => d.Quantity), ShowQuantityAs.None)}";
                result.Quantity = details.Sum(d => d.Quantity);
                results.Add(result);
            }

            return results;
        }

        [HttpPost]
        public async Task<Order> CreateOrder([FromBody]ICollection<OrderViewModel> orderItems)
        {
            var order = new Order();

            foreach (var item in orderItems)
            {
                var detail = new OrderDetail
                {
                    Amount = item.Amount,
                    Gst = item.Gst,
                    ProductId = item.Product.Id,
                    Quantity = item.Quantity,
                    SizeId = item.Size.Id
                };

                foreach (var topping in item.Toppings)
                {
                    detail.OrderDetailToppings.Add(new OrderDetailTopping { ToppingId = topping.Id });
                }

                order.OrderDetails.Add(detail);
            }

            order.TotalAmount = order.OrderDetails.Sum(od => od.Amount + od.Gst);

            this.db.Orders.Add(order);
            await this.db.SaveChangesAsync();
            return order;
        }
    }
}
