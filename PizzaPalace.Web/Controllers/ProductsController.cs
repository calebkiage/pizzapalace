﻿using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PizzaPalace.Web.Context;
using PizzaPalace.Web.Entities;
using PizzaPalace.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaPalace.Web.Controllers
{

    [Route("api/[controller]")]
    public class ProductsController
    {
        private PizzaPalaceDbContext db;

        public ProductsController(PizzaPalaceDbContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public IQueryable<Product> GetAll()
        {
            return this.db.Products.Include(p=> p.ProductSizes);
        }

        [HttpGet("{id}")]
        public Task<Product> Get(int id)
        {
            return this.db.Products.FindAsync(id);
        }

        [HttpPost("by-simple-name")]
        public OrderViewModel GetBySimpleName([FromBody]string simpleName)
        {
            var parts = simpleName?.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries)?.Select(s=> s?.Trim());
            var sizeStr = parts.First();
            var toppingList = parts.Last().Split(new[] { ","}, StringSplitOptions.RemoveEmptyEntries).Select(s=> s?.Trim());

            // The global size...includes pizza and toppings.
            var size = this.db.Sizes.Include(s=> s.ProductSizes).ThenInclude(ps=> ps.Product).Single(s => s.Name == sizeStr);

            // Get the pizza.
            var pizza = size.ProductSizes.Single(ps => ps.Product.Type == ProductType.Pizza);

            // Get the toppings.
            var toppings = size.ProductSizes.Where(ps => ps.Product.Type != ProductType.Pizza)
                .Where(ps => toppingList.Contains(ps.Product.Name));

            var result = new OrderViewModel
            {
                Product = new ProductViewModel { Id = pizza.ProductId, Name = pizza.Product.Name, Type = pizza.Product.Type },
                Size = new SizeViewModel { Id = pizza.SizeId, Name = pizza.Size.Name }
            };
            // Add the toppings.
            result.Toppings.AddRange(toppings.Select(t => new ProductViewModel { Id = t.ProductId, Name = t.Product.Name, Type = t.Product.Type }));
            result.Amount = pizza.Amount + toppings.Select(t=> t.Amount).Sum();

            // Round up to the nearest cent.
            result.Gst = Math.Ceiling(result.Amount * 5) * 0.01m;

            result.Label = $"{pizza.Size?.Name}, {result.Toppings.Count.ToWords().Transform(To.TitleCase)} Topping Pizza - {result.Toppings.Select(t=> t.Name).Humanize()}";
            return result;
        }
    }
}
