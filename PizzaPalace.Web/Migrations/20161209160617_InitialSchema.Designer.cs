﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using PizzaPalace.Web.Context;
using PizzaPalace.Web.Entities;

namespace PizzaPalace.Web.Migrations
{
    [DbContext(typeof(PizzaPalaceDbContext))]
    [Migration("20161209160617_InitialSchema")]
    partial class InitialSchema
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("PizzaPalace.Web.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("TotalAmount");

                    b.HasKey("Id");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.OrderDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount");

                    b.Property<decimal>("Gst");

                    b.Property<int>("OrderId");

                    b.Property<int>("ProductId");

                    b.Property<double>("Quantity");

                    b.Property<int>("SizeId");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("ProductId");

                    b.HasIndex("SizeId");

                    b.ToTable("OrderDetails");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.OrderDetailTopping", b =>
                {
                    b.Property<int>("OrderDetailId");

                    b.Property<int>("ToppingId");

                    b.HasKey("OrderDetailId", "ToppingId");

                    b.ToTable("OrderDetailTopping");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.ProductSize", b =>
                {
                    b.Property<int>("ProductId");

                    b.Property<int>("SizeId");

                    b.Property<decimal>("Amount");

                    b.HasKey("ProductId", "SizeId");

                    b.HasIndex("SizeId");

                    b.ToTable("ProductSize");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.Size", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Sizes");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.OrderDetail", b =>
                {
                    b.HasOne("PizzaPalace.Web.Entities.Order", "Order")
                        .WithMany("OrderDetails")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PizzaPalace.Web.Entities.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PizzaPalace.Web.Entities.Size", "Size")
                        .WithMany()
                        .HasForeignKey("SizeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.OrderDetailTopping", b =>
                {
                    b.HasOne("PizzaPalace.Web.Entities.OrderDetail", "OrderDetail")
                        .WithMany("OrderDetailToppings")
                        .HasForeignKey("OrderDetailId");

                    b.HasOne("PizzaPalace.Web.Entities.Product", "Topping")
                        .WithMany("OrderDetailToppings")
                        .HasForeignKey("OrderDetailId");
                });

            modelBuilder.Entity("PizzaPalace.Web.Entities.ProductSize", b =>
                {
                    b.HasOne("PizzaPalace.Web.Entities.Product", "Product")
                        .WithMany("ProductSizes")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PizzaPalace.Web.Entities.Size", "Size")
                        .WithMany("ProductSizes")
                        .HasForeignKey("SizeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
