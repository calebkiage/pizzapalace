﻿using System.Collections.Generic;

namespace PizzaPalace.Web.ViewModels
{
    public class OrderViewModel
    {
        public decimal Amount { get; set; }

        public decimal Gst { get; set; }

        public string Label { get; set; }

        public ProductViewModel Product { get; set; }

        public double Quantity { get; set; } = 1;

        public SizeViewModel Size { get; set; }

        public List<ProductViewModel> Toppings { get; private set; } = new List<ProductViewModel>();
    }
}
