﻿namespace PizzaPalace.Web.ViewModels
{
    public class SizeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
