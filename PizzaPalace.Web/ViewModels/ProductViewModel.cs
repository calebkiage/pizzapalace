﻿using PizzaPalace.Web.Entities;

namespace PizzaPalace.Web.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ProductType Type { get; set; }
    }
}
