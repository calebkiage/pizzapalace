# README #

### Frameworks used. ###

1. [ASP.NET Core](https://www.asp.net/core)
2. [Entity Framework Core](https://docs.microsoft.com/en-us/ef/)
3. [WPF](https://msdn.microsoft.com/en-us/library/aa970268(v=vs.110).aspx)

### Setup. ###

Build Requirements:

* Visual Studio 2015
* ASP.NET Core SDK