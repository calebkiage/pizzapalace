﻿using Caliburn.Micro;
using Newtonsoft.Json.Linq;
using PizzaPalace.Client.Contracts;
using PizzaPalace.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace PizzaPalace.Client.Order
{
    public sealed class CreateOrderViewModel : Screen, IMainScreenTabItem
    {
        private readonly CompleteOrderViewModel completeOrderVm;
        private readonly HttpClient client;
        private readonly IWindowManager windowManager;

        public CreateOrderViewModel(CompleteOrderViewModel completeOrderVm, HttpClient client, IWindowManager windowManager)
        {
            this.client = client;
            this.completeOrderVm = completeOrderVm;
            this.DisplayName = "Create Order";
            this.OrderDetails = new BindableCollection<OrderViewModel>();
            this.OrderDetailsRaw = new BindableCollection<OrderViewModel>();
            this.windowManager = windowManager;
        }

        public bool OrderProgressIsVisible { get; set; }

        public decimal Gst { get; set; }

        public string Order { get; set; }

        public BindableCollection<OrderViewModel> OrderDetails { get; }

        public BindableCollection<OrderViewModel> OrderDetailsRaw { get; }

        public decimal SubTotal { get; set; }

        public decimal Total { get; set; }

        public bool CanAddOrder(bool addingOrderIsVisible, string order)
        {
            return !addingOrderIsVisible && !string.IsNullOrWhiteSpace(order);
        }

        public async void AddOrder(bool addingOrderIsVisible, string order)
        {
            this.OrderProgressIsVisible = true;

            try
            {
                // Fetch order details.
                var response = await this.client.PostAsJsonAsync("/api/products/by-simple-name", this.Order);
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsAsync<OrderViewModel>();

                this.Order = string.Empty;
                this.OrderDetailsRaw.Add(result);

                // First, we group.
                var grouped = this.OrderDetailsRaw.GroupBy(od => od.Label)
                    .Select(g =>
                    {
                        var details = new OrderViewModel
                        {
                            Amount = g.Sum(i => i.Amount),
                            Gst = g.Sum(i => i.Gst),
                            Label = g.Select(i => i.Label).First(),
                            Product = g.Select(i => i.Product).First(),
                            Quantity = g.Count(),
                            Size = g.Select(i => i.Size).First()
                        };
                        details.Toppings.AddRange(g.Select(i => i.Toppings).First());
                        return details;
                    });
                this.OrderDetails.Clear();
                this.OrderDetails.AddRange(grouped);

                this.Gst = this.OrderDetailsRaw.Sum(d => d.Gst);
                this.SubTotal = this.OrderDetailsRaw.Sum(d => d.Amount);
                this.Total = this.SubTotal + this.Gst;
            }
            catch (Exception e)
            {
                // Handle error.
            }
            finally
            {
                this.OrderProgressIsVisible = false;
            }
        }

        public async void CompleteOrder()
        {
            if (!this.OrderDetailsRaw.Any())
            {
                return;
            }

            try
            {
                // Show checkout screen.
                this.completeOrderVm.TotalAmount = this.Total;
                if (this.windowManager.ShowDialog(this.completeOrderVm) == true)
                {
                    // If done is clicked, send data to the server.
                    var response = await this.client.PostAsJsonAsync<IEnumerable<OrderViewModel>>("api/orders", this.OrderDetails);
                    response.EnsureSuccessStatusCode();

                    var result = await response.Content.ReadAsAsync<JObject>();
                    if (result != null)
                    {
                        // Clear lists.
                        this.OrderDetailsRaw.Clear();
                        this.OrderDetails.Clear();
                        // Show message box.
                    }
                }
            }
            catch(Exception e)
            {
                // Handle error.
            }
            finally
            {

            }
        }
    }
}
