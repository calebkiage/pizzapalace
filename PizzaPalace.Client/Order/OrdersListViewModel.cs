﻿using Caliburn.Micro;
using PizzaPalace.Client.Contracts;
using PizzaPalace.Client.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace PizzaPalace.Client.Order
{
    public sealed class OrdersListViewModel : Screen, IMainScreenTabItem
    {
        HttpClient client;

        public OrdersListViewModel(HttpClient client)
        {
            this.client = client;
            this.DisplayName = "Orders";
            this.Orders = new BindableCollection<OrderViewModel>();
        }

        public BindableCollection<OrderViewModel> Orders { get; }

        protected override async void OnActivate()
        {
            try
            {
                var response = await this.client.GetAsync("api/orders");
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsAsync<IEnumerable<OrderViewModel>>();
                this.Orders.Clear();
                this.Orders.AddRange(result);
            }
            catch (Exception e)
            {
                // Handle error.
            }
            finally
            {

            }
        }
    }
}
