﻿using Caliburn.Micro;
using System;

namespace PizzaPalace.Client.Order
{
    public sealed class CompleteOrderViewModel : Screen
    {
        public CompleteOrderViewModel()
        {
            this.DisplayName = "Complete Order";
            this.PaidAmountString = "0";
        }

        public decimal Change {
            get {
                decimal paid;
                if (!decimal.TryParse(this.PaidAmountString, out paid))
                {
                    paid = 0;
                }
                return paid - this.TotalAmount;
            }
        }

        public string ChangeString { get { return $"${this.Change.ToString("N")}"; } }

        public string PaidAmountString { get; set; }

        public decimal TotalAmount { get; set; }

        public string TotalAmountString
        {
            get { return $"${this.TotalAmount.ToString("N")}"; }
        }

        public bool CanComplete(string totalAmountString, string paidAmountString)
        {
            decimal paid;
            if (!decimal.TryParse(this.PaidAmountString, out paid))
            {
                paid = 0;
            }
            return (paid - this.TotalAmount) >= 0;
        }

        public void Complete(string totalAmountString, string paidAmountString)
        {
            if (this.Change < 0)
            {
                return;
            }

            this.TryClose(true);
        }
    }
}
