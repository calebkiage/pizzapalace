﻿using MahApps.Metro.Controls;

namespace PizzaPalace.Client.Shell
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ShellView : MetroWindow
    {
        public ShellView()
        {
            InitializeComponent();
        }
    }
}
