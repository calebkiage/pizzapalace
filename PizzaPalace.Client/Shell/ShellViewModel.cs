using System;
using Caliburn.Micro;
using System.Collections.Generic;
using PizzaPalace.Client.Contracts;

namespace PizzaPalace.Client.Shell {
    public sealed class ShellViewModel : Conductor<IMainScreenTabItem>.Collection.OneActive {

        public ShellViewModel(IEnumerable<IMainScreenTabItem> tabs)
        {
            this.DisplayName = "Pizza Palace";
            this.Items.AddRange(tabs);
        }
    }
}