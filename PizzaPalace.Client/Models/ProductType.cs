﻿namespace PizzaPalace.Client.Models
{
    public enum ProductType
    {
        Pizza,
        BasicTopping,
        DeluxeTopping
    }
}
