namespace PizzaPalace.Client
{
    using System.Windows;
    using Autofac;
    using Caliburn.Micro;
    using Caliburn.Micro.Autofac;
    using Shell;
    using System.Linq;
    using System.Net.Http;
    using System;
    using System.Net.Http.Headers;

    public class AppBootstrapper : AutofacBootstrapper<ShellViewModel> {
        public AppBootstrapper() {
            Initialize();
        }

        protected override void ConfigureBootstrapper()
        {
            base.ConfigureBootstrapper();

            this.AutoSubscribeEventAggegatorHandlers = true;
            this.EnforceNamespaceConvention = false;
        }

        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            //  register view models
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
              //  must be a type with a name that ends with ViewModel
              .Where(type => type.Name.EndsWith("ViewModel"))
              //  must be in a namespace ending with ViewModels
              .Where(type => EnforceNamespaceConvention ? (!(string.IsNullOrWhiteSpace(type.Namespace)) && type.Namespace.EndsWith("ViewModels")) : true)
              //  must implement INotifyPropertyChanged (deriving from PropertyChangedBase will statisfy this)
              .Where(type => type.GetInterface(ViewModelBaseType.Name, false) != null)
              //  registered as self
              .AsSelf()
              .AsImplementedInterfaces()
              //  always create a new one
              .InstancePerDependency();
            builder.Register(c => {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:5000/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                return client;
            }).AsSelf().SingleInstance();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}